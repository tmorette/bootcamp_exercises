package org.academiadecodigo.rHASHtafaris.uniqueclass;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        String string = "five two one one two three one four two";
        StringsArray newStringArray = new StringsArray(string);

        System.out.println(newStringArray.getHashSet());

        for (String s : newStringArray) {
            System.out.print(s + " ");
        }
    }
}

class StringsArray implements Iterable<String>{

    private Set<String> hashSet;

    StringsArray(String string) {
        hashSet = new HashSet<>();
        Collections.addAll(hashSet, string.split(" "));
    }

    public Set<String> getHashSet() {
        return hashSet;
    }

    @Override
    public Iterator<String> iterator() {
        return hashSet.iterator();
    }
}
