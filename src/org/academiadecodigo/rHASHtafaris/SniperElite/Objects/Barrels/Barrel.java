package org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Barrels;

import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Destroyable;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.GameObject;

public class Barrel extends GameObject implements Destroyable {

    BarrelType barrelType;
    private int currentDamage;
    private boolean destroyed;

    public Barrel () {
        barrelType = BarrelType.getBarrelType();
        currentDamage = barrelType.getMaxDamage();
    }

    public int getLife () {
        return currentDamage;
    }

    @Override
    public void hit(int damage) {

        if (damage >= currentDamage) {
            currentDamage = 0;
            destroyBarrel();
        }

        currentDamage -= damage;
        System.out.println(getMessage());
    }

    public void destroyBarrel() {
        this.destroyed = true;
    }

    @Override
    public boolean isDestroyed() {
        return destroyed;
    }

    @Override
    public String getMessage() {
        if (isDestroyed()) {
            return "---barrel is destroyed---\n";
        }

        return "---the " + barrelType + " barrel have " + currentDamage + " life---\n";
    }
}
