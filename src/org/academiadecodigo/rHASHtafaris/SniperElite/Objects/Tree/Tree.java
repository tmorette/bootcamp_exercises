package org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Tree;

import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.GameObject;

public class Tree extends GameObject {

    @Override
    public String getMessage() {
        return "---that's a tree---\n";
    }
}
