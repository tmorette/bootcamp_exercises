package org.academiadecodigo.rHASHtafaris.SniperElite.Objects;

public interface Destroyable {
    void hit (int damage);

    boolean isDestroyed();

    int getLife();
}
