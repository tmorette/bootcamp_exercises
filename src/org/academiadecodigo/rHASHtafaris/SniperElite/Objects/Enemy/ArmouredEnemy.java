package org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Enemy;

public class ArmouredEnemy extends Enemy {
    private int armour;

    public ArmouredEnemy() {
        armour = (int) ((Math.random() * 30) + 15);
    }

    public int getArmour() {
        return armour;
    }

    @Override
    public void hit(int damage) {

        if (damage == 100) {
            super.hit(damage);
            return;
        }

        if (armour > damage) {
            armour -= damage;
            System.out.println(getMessage());
            return;
        }

        int healthDamage = Math.abs(armour - damage);
        armour = 0;
        super.hit(healthDamage);
    }

    @Override
    public String getMessage() {
        if (armour > 0) {
            return "Enemy Solider: shit... he hit my armour, but i still have "
                    + getArmour() + " armour and " + getLife() + " health!\n";
        }

        return super.getMessage();
    }
}
