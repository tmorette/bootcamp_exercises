package org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Enemy;

public enum BodyParts {
    HEAD,
    ARMS,
    LEGS,
    BODY;

    public static BodyParts getBodyPart() {
        int random = (int) (Math.random() * 100);

        if (random < 10) {
            return BodyParts.HEAD;
        }

        if (random < 30) {
            return BodyParts.ARMS;
        }

        if (random < 50) {
            return BodyParts.LEGS;
        }

        return BodyParts.BODY;
    }
}
