package org.academiadecodigo.rHASHtafaris.SniperElite;

import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Barrels.Barrel;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Destroyable;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Enemy.ArmouredEnemy;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Enemy.Enemy;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Enemy.SoldierEnemy;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.GameObject;
import org.academiadecodigo.rHASHtafaris.SniperElite.Objects.Tree.Tree;
import org.academiadecodigo.rHASHtafaris.SniperElite.Sniper.SniperRifle;

public class Game {

    private GameObject[] gameObject;

    private void createObjects(int numberOfObjects) {

        gameObject = new GameObject[numberOfObjects];

        for (int i = 0; i < numberOfObjects; i++) {

            double random = Math.random();

            if (random < 0.1) {
                gameObject[i] = new Tree();
                continue;
            }

            if (random < 0.25) {
                gameObject[i] = new Barrel();
                continue;
            }

            if (random < 0.5) {
                gameObject[i] = new ArmouredEnemy();
                continue;
            }

            gameObject[i] = new SoldierEnemy();
        }
    }

    public void start() {
        createObjects(10);
        SniperRifle sniper = new SniperRifle();

        for (GameObject g : gameObject) {
            if (!(g instanceof Destroyable)) {
                System.out.println(g.getMessage());
                continue;
            }

            sniper.shotEnemy((Destroyable) g);
            System.out.println();

        }
    }
}
