package org.academiadecodigo.rHASHtafaris.javaio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputOuputStream {
    public static void main(String[] args) {

        CopyFile copyFile = new CopyFile();
        copyFile.copy("/Users/codecadet/Documents/Projects/resources/file.txt", "/Users/codecadet/Documents/Projects/resources/new_file.txt");
    }
}

class CopyFile {

    void copy(String fileInput, String fileOutput) {
        int dataRead;
        byte[] buffer = new byte[2048];

        try (
                FileInputStream inputStream = new FileInputStream(fileInput);
                FileOutputStream outputStream = new FileOutputStream(fileOutput)
        ) {
            while ((dataRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, dataRead);
                System.out.println("data copy ok");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
