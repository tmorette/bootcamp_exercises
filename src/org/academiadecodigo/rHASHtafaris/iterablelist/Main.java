package org.academiadecodigo.rHASHtafaris.iterablelist;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        Range numbers = new Range(1, 5);

        for(Integer n : numbers) {
            System.out.println(n);
        }
    }
}

class Range implements Iterable<Integer> {

    private int max;
    private int min;

    Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new RangeIterator(max, min);
    }
}

class RangeIterator implements Iterator<Integer> {

    private int max;
    private int min;

    RangeIterator (int max, int min) {
        this.max = max;
        this.min = min;
    }

    @Override
    public boolean hasNext() {
        if (min <= max) {
            return true;
        }

        return false;
    }

    @Override
    public Integer next() {
        return min++;
    }
}
