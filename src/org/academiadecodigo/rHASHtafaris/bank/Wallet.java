package org.academiadecodigo.rHASHtafaris.bank;

public class Wallet {
    private double ammountMoney;
    private double limit;

    public Wallet (double limit) {
        this.limit = limit;
    }

    public double getAmmountMoney() {
        return ammountMoney;
    }

    public double checkLimit() {
        return limit - ammountMoney;
    }

    public void withdraw (double ammount) {
        if (this.ammountMoney > ammount) {
            ammountMoney -= ammount;
        }
    }

    public void deposit (double ammount) {
        this.ammountMoney += ammount;
    }
}
