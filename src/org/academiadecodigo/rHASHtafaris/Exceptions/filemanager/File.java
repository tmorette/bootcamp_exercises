package org.academiadecodigo.rHASHtafaris.Exceptions.filemanager;

public class File {
    private String name;

    File(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
