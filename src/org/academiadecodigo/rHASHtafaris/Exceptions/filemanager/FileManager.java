package org.academiadecodigo.rHASHtafaris.Exceptions.filemanager;

import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.FileNotFoundException;
import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.NotEnoughSpaceException;

public class FileManager {
    private boolean loggedIn;
    private int arrayIndex;

    private File[] files;

    public FileManager() {
        files = new File[1];
        arrayIndex = 0;
    }

    public void login() {
        if (!loggedIn) {
            loggedIn = true;
        }
    }

    public void logout() {
        if (loggedIn) {
            loggedIn = false;
        }
    }

    public void checkPermission() throws NotEnoughPermissionsException {
        if (!loggedIn) {
            throw new NotEnoughPermissionsException();
        }
    }

    public File getFiles(String fileName) throws FileNotFoundException, NotEnoughPermissionsException {
        checkPermission();
        File file = null;

        for (File value : this.files) {
            if (value.getName().equals(fileName)) {
                file = value;
            }
        }

        if (file == null) {
            throw new FileNotFoundException();
        }

        return file;

    }

    public void createFile(String fileName) throws NotEnoughPermissionsException, NotEnoughSpaceException {
        if (!loggedIn) {
            throw new NotEnoughPermissionsException();
        }

        if (files[files.length - 1] != null) {
            throw new NotEnoughSpaceException();
        }

        files[arrayIndex] = new File(fileName);
        arrayIndex++;
    }

    public void deleteFile(String name) {
    }

    /**
     * Double the size of the array file
     */
    public void increaseSize() {
        File[] newFiles = new File[files.length * 2];

        System.arraycopy(files, 0, newFiles, 0, files.length);

        files = newFiles;

        for (File fileName : files) {
            System.out.println(fileName);
        }
    }

    public void print() {
        for (File fileName : files) {
            if (fileName != null) {
                System.out.println(fileName.getName());
            }
        }
    }
}
