package org.academiadecodigo.rHASHtafaris.Exceptions;

import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.FileNotFoundException;
import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.rHASHtafaris.Exceptions.exceptions.NotEnoughSpaceException;
import org.academiadecodigo.rHASHtafaris.Exceptions.filemanager.FileManager;

public class Main {

    public static void main(String[] args) {
        FileManager fileManager = new FileManager();

        try {
            fileManager.login();
            fileManager.createFile("test");
            fileManager.getFiles("test 2");
            fileManager.createFile("test 2");
            fileManager.createFile("test 3");
            fileManager.print();
        } catch (NotEnoughSpaceException e) {
             fileManager.increaseSize();
        } catch (NotEnoughPermissionsException e) {
            System.out.println("you're not allowed to make this operation");
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        }

        fileManager.print();
    }
}
