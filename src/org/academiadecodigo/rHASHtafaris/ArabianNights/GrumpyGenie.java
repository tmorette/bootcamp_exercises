package org.academiadecodigo.rHASHtafaris.ArabianNights;

public class GrumpyGenie extends Genie {

    public GrumpyGenie() {
        super();
    }

    @Override
    public void grantWish() {

        System.out.println("I'm the grumpy genie.");

        if (super.getWishes() > 0) {
            System.out.println("wish granted");
            super.wishGranted();
            return;
        }

        System.out.println("i cannot grant you anymore wishes.");

    }

}
