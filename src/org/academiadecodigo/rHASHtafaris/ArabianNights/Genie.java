package org.academiadecodigo.rHASHtafaris.ArabianNights;

public abstract class Genie {
    private int wishes;
    private int wishGranted;


    public Genie() {
        this.wishes = 3;
    }

    public int getWishes() {
        return wishes;
    }

    public int getWishGranted() {
        return wishGranted;
    }

    public void wishGranted() {
        wishes--;
    }

    public abstract void grantWish();
}
