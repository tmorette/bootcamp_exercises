package org.academiadecodigo.rHASHtafaris.ArabianNights;

public class RecyclableDemon extends Genie {

    boolean recycled = false;

    public RecyclableDemon() {
        super();
    }

    @Override
    public void grantWish() {
        int start = 0;

        System.out.println("I'm a demon and will realize all wishes.");

        while (super.getWishes() > start) {
            System.out.println("wish granted");
            start++;
        }
    }
}
