package org.academiadecodigo.rHASHtafaris.histogram;

import java.util.*;

public class InheritanceClass {
    public static void main(String[] args) {

        String phrase = "this is just a test test just this";

        Histogram histogram = new Histogram(phrase);

        for (String s : histogram) {
            System.out.println(s + ": " + histogram.get(s));
        }

    }
}

class Histogram extends HashMap<String, Integer> implements Iterable<String> {

    Histogram(String string) {
        for (String s : string.split(" ")) {
            if (!containsKey(s)) {
                put(s, 1);
                continue;
            }

            put(s, get(s) + 1);
        }
    }

    @Override
    public Iterator<String> iterator() {
        return keySet().iterator();
    }
}
