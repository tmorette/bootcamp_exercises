package org.academiadecodigo.rHASHtafaris.rockpaperscissors;

public class Player {
    private String name;
    private int victories;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getVictories() {
        return victories;
    }

    public void win () {
        this.victories++;
    }

    public HandTypes randomResult () {
        return HandTypes.getRandomType();
    }
}
