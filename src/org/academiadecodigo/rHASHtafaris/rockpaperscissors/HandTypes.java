package org.academiadecodigo.rHASHtafaris.rockpaperscissors;

public enum HandTypes {
    ROCK,
    PAPER,
    SCISSORS;

    public static HandTypes getRandomType() {

        int index = (int) (Math.random() * HandTypes.values().length);
        return HandTypes.values()[index];
    }
}
