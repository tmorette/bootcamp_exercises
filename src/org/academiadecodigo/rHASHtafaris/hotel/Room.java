package org.academiadecodigo.rHASHtafaris.hotel;

public class Room {
    private boolean isFree;
    private int roomNumber;

    public Room (int roomNumber){
        this.roomNumber = roomNumber;

        changeVacancy();
    }

    public boolean isFree() {
        return isFree;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber (int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void changeVacancy () {
        if (!isFree) {
            isFree = true;
        }

        isFree = false;
    }
}
