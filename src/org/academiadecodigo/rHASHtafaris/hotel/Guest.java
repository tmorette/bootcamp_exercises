package org.academiadecodigo.rHASHtafaris.hotel;

public class Guest {
    private String name;
    private Room room;
    private boolean checkedIn;

    public Guest (String name) {
        this.name = name;
    }

    public void checkIn (Hotel hotel) {
        hotel.verifyRoom(this);
    }

    public String getName() {
        return name;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public int getRoomNumber () {
        return room.getRoomNumber();
    }
    public void checkedOut (Hotel hotel) {}
}
