package org.academiadecodigo.rHASHtafaris.guessnumber;

public class Main {
    public static void main(String[] args) {

        Host host = new Host();

        Player playerOne = new Player("thiago");
        Player playerTwo = new Player("kamila");

        host.startGame(10, 20, playerOne, playerTwo);

        // TODO: 23/01/2020 define both upper and lower limit
        // TODO: 23/01/2020 define a maximum number of rounds. if no player guesses right, there's no winner
        // TODO: 23/01/2020 players does not repeat itself when guessing
        // TODO: 23/01/2020 players don't repeat themselves nor other players
    }
}
