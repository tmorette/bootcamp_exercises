package org.academiadecodigo.rHASHtafaris.guessnumber;

public class Player {
    private String name;

    public Player (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public int getRandomNumber (int limit) {
        return Random.generateRandomNumber(limit);
    }
}
